# KJMon
## What is it?
A simple s-100 homebrew monitor (based on bits of Nas-Bug) to view, modify and execute memory.

Additional routines are added after 400H to support the Intel 8272 Floppy Disk Controller.
## For?
Designed for a Compupro CPU-Z, RAM17, DISK1 and System Support1.  

![My S-100 CompuPro rig](/assets/PXL_20240116_214851083.MP.jpg "My S-100 Rig")

## How?
Go to the releases for this project at https://gitlab.com/keithmarsh/kjmon/-/releases and click on Generated ROM Binary.  

Alternatively, to live on the edge, go to pipelines at 
https://gitlab.com/keithmarsh/kjmon/-/pipelines and click on the download arrow on the latest build.  The intex hex is in kjmon.hex.

Burn this monitor to a 2K AT28C16 EEPROM and put it in U17 LOW EPROM socket of the System Support 1.  Disable 
0000-3FFF on RAM17.

The ROM will occupy 0000-07FF and uses memory at 4000-7FFF.  
Vary the boards below (**CAPS** means changed) from their DISK1 Getting Started CP/M boot positions.

### CPU-Z

S1 | Set | Meaning
---|-----|-----
1  | **ON**  | Wait for On-board Memory
2  | **ON**  | Wait for I/O
3  | **ON**  | Wait for Memory
4  | **ON**  | Wait for instruction fetch
5  | **ON**  | 4Mhz:Off 2MHz:On
8  | OFF  | ON=Power-On-Jump enabled
****
S3 | Set | Meaning
----|-----|---
 1  | **ON**  | SOCKET ADDRESS BIT A15 (OFF=1!)
 2  | **ON**  | SOCKET ADDRESS BIT A14
 3  | **ON**  | SOCKET ADDRESS BIT A13
 4  | **ON**  | SOCKET ADDRESS BIT A12
 5  | ON      | SOCKET DISABLE (WHEN ON)
 6  | OFF     | SOCKET BASE PAGE ONLY (WHEN ON)
 7  | **ON**  | ON=JUMP-ON-RESET ENABLE 
 8  | OFF     | INTERRUPT ENABLE FOR VECTORED INTERRUPTS

 ### DISK1

S1 | Set | Meaning
----|-----|---
 1  | ON  | wait enable
 2  | OFF | 
 3  | OFF | Serial available, no motor control
 4  | **ON** | ON=BOOT INHIBIT EPROM
 5  | OFF | Priority Bit 3 (OFF=1!)
 6  | OFF | Priority Bit 2
 7  | OFF | Priority Bit 1
 8  | OFF | Priority Bit 0

S2 | Set | Meaning
----|------|---
 1  | OFF | Boot Addr (See below)
 2  | ON  | Boot Addr
 3  | OFF | PORT A7 - C0H (OFF=1!)
 4  | OFF | PORT A6
 5  | ON  | PORT A5
 6  | ON  | PORT A4
 7  | ON  | PORT A3 (5.25 CCH OFF!)
 8  | ON  | PORT A2 (5.25 CCH OFF!)

 J16-A 2 wait States
 J16-B 3 wait states
 J16-NC 4 wait states

#### BOOT

J17 | S2-1 | S2-2 | Boot Addr | Routine | Desc
----|------|------|-----------|---------|---
B | ON  | ON  | 000H | 0 | 8086 Interfacer 1/2 00H
B | ON  | OFF | 100H | 1 | 8086 DISK1 Serial
B | OFF | ON  | 200H | 2 | 8086 System Support 1 50H 9600
B | OFF | OFF | 300H | 3 | 8086 Interfacer 3/4 10H 9600
A | ON  | ON  | 400H | 4 | 8080 Interfacer 1/2 00H
A | ON  | OFF | 500H | 5 | 8080 DISK1 Serial
A | OFF | ON  | 600H | 6 | 8080 System Support 1 50H 9600
A | OFF | OFF | 700H | 7 | 8080 Interfacer 3/4 10H 9600

[!NOTE]
The older 171D version does not support 8086, and does not have J17, so it's ROM is different to 171F
in that only 000H to 300H are provided, and these are 8080 versions.  The code is the same though.

 ### RAM17

S1 | Set | Meaning
----|------|---
 1  | OFF | Not Front Panel
 2  | ON  | F800-FFFF
 3  | ON  | F000-F7FF
 4  | ON  | E800-EFFF
 5  | ON  | E000-E7FF
 6  | ON  | C000-FFFF Enables 2-5
 7  | ON  | 8000-BFFF
 8  | ON  | 4000-7FFF
 9  | **OFF**  | 0000-3FFF
 10 | ON  | Not Front Panel

S2  | Set | Meaning
----|-----|---
 1  | ON  | Global
 10 | **OFF** | ON=Obey Phantom

 ### System Support 1

S1 | Set | Meaning
----|------|---
 1  | **OFF** | Wait States = 0000=None 0001=1
 2  | **OFF** | Wait States = 0000=None
 3  | **OFF** | Wait States = 0000=None
 4  | **ON**  | Wait States = 0000=None
 5  | **OFF** | ON=Disable ROM
 6  | **ON**  | ON=Disable extended addressing
 7  | **OFF** | ON=Allow PHANTOM to disable memory
 8  | **OFF** | ON=Allow PHANTOM to enable memory

S3 | Set | Meaning
----|------|---
 1  | ON  | ROM Addr 15 OFF=1!
 2  | ON  | ROM Addr 14 OFF=1!
 3  | ON  | ROM Addr 13 OFF=1!
 4  | ON  | ROM Addr 12 OFF=1!
 5  | ON  | I/O Addr 7 OFF=1!
 6  | OFF | I/O Addr 6 OFF=1!
 7  | ON  | I/O Addr 5 OFF=1!
 8  | OFF | I/O Addr 4 OFF=1!

 J13 Z-C


## Why?
I don't have any CP/M disks, and I don't know if anything is working.  I had to write this:
1. to see if my cards were working and I could run code
2. to see if my DISK1 could control a floppy
3. to learn how to build my own CP/M BIOS for my system from the ground up.
4. build a 5.25" boot disk using this BIOS
4. then I can discard the rom and switch to CP/M!  Hurrah.
## Commands

    L AAAA BBBB       - List memory from AAAA to BBBB
    
    M AAAA            - Modify memory till . entered

    C AAAA BBBB NN    - Copy AAAA to BBBB for NN bytes

    E AAAA            - Execute AAAA

    X                 - Ingest Intel Hex until complete

    B                 - binary echo pressed key  till ESC pressed.  Used for debugging serial.

    D AAAA            - Set DMA address to AAAA

    I                 - Initialise the disk system

    T tt n            - Change the number of tracks to TT and N to nn and re-init

    S NN              - Seek to track NN

    F                 - Format a disk

    R NN MM AAAA      - Read a sector of data from track NN, sector MM to address AAAA

    W NN MM AAAA      - (TODO) Write a sector of data to track NN sector MM from address AAAA


## Interface
![A serial session](/assets/2024-01-16 21_50_33-COM1.jpg "A serial session")

## Log Book
### 19-02-2024

Trying to diagnose why SEEK isn't working on OKIDATA GM3305BU drive, when the drive works on a BBC B (and I'm sure it used to seek a while back on the DISK1).  Seek Rate time is set to 8mS in DISK1.  However, modifying the DISK1 for 5.25" halves the clock speed, so this is double.  Lets try reducing it to "4ms" - no joy.
Let's check the original values, which I'm sure used to work.

    SRT 	EQU     16-3       ; STEP RATE TIMER 8MS
    HUT 	EQU     240/16     ; HEAD UNLOAD TIMER 240MS
    HLTT	EQU     (35+1)/2   ; HEAD LOAD TIMER 35MS

Let's rollback to those values - didn't work.

### 20-02-2024

Switched from floppy to Lotharek to eliminate an uncertainty - seek works now but Read is the same.
Checked all minifloppy mods made to DISK1 - all look good.
Moved J15 from AC (Ready supported) to CB (does not drive ready line).  There is a change -
On a Read track 0 Sector 1, Status register 2 reports end of cylinder EN (D7) now instead of Missing address mark MA (D0).  Interesting.

### 21-02-2024

Can't repro EN, both J15 now give MA.  Added I and H commands.

### 25-02-2024

Replaced the NEC 765AC with an 8272A. I can now READ ID (cmd 0AH) 
successfully and get the current track and sector the head is over.  
But a Read track 0 sector 1 returns an EN (D7) all the time now.  
I'm using an Osborne disk of 40T, 10Sec, 256bytes/sector (N:1), 
Single density (FM).  I think the gap is 8, but no idea.  I made 
sure DTL is 0FFH, and tried EOT or 1, 2, 9 and 10.  I had the read 
hang once, which makes me think that might of worked, but :-/

### 26-02-2024

Although the read fails with EN, the data from the track is actually 
transferred to memory correctly.  This is fantastic as it means the 
DMA circuitry is working.  Now to work out whats causing the EN.
I*Future Keith here - I think the EN is because EOT is set to 77 rather than the number of sectors to read, i.e. 1 or 2.*

#### Terminators

I think we're getting somewhere.  I just noticed the READDATA pin 
from the floppy interface has a 150 Ohm pull-up resistor to 5V.  
Reading [The Floppy User Guide](https://archive.org/details/the-floppy-user-guide), 
a great source of information, it says at the start of Chapter 8, 
The Floppy Bus, that old style termination is 150 ohm.  However, my 
floppies also have this terminator on.  My OKIDATA GM3305BU can 
toggle the terminator with a DIP switch, and when I turn it off, 
hey presto, seeking starts to work!

### 29-05-2024

#### CCS 2422 FDC Rev 1

I'm thinking the Disk1 is broken, or the 5.25" mod does not work.  
I do have a Shugart 801 8" floppy disk available now, so I could 
flip it back.  However, the 2422 handles both 5.25" and 8", so it 
might be fun to switch KJMon over to use that (or alongside the 
Disk1 support)

The CCS 2422 requires

* 0000-00FF as scratch RAM.
* F000-F7HH for the ROM
* Power on jump to F000
* 5.25 SSSD 128 bytes/sector, 18 sectors per track, 40 tracks
* 8.00 SSSD 128 bytes/sector, 26 sectors per track, 77 tracks

I'm going to do this work in a new project [ccs-2422](https://gitlab.com/keithmarsh/ccs-2422).  See you there!

### 01-11-2024

Right, I'm back.  The CCS 2422 worked nicely.

I've got another DISK1, but an older revision, 171D, that doesn't support 8086, but it hasn't been converted to 5.25".  I've got my GoTek working as an 8" with the [Altos](https://github.com/keithmarsh/altos-acs8000), so I hope to get this DISK1 working with the GoTek.

I jumped straight in an tried the CP/M Boot ROM on the DISK1, but it fails a disk command somewhere, so I'm going back to KJMon to debug the new card.  I'm spending today documenting the precise mods needed to the stock Compupro config for CP/M.

I've got KJMon outputing it's welcome, but then it halts, indicating it's executing FFH somewhere, which is could be a RAM config setting.

### 03-11-2024

Moved RAM use from RAM17 card to the CPU-Z onbaord RAM in U17 HIGH.
Added O log level command.
Fixed a memory buffer error.
Added an interrupt status logger.  This allows me to see that Read Sector and Read Id commands are never completing once the command bytes are sent, the interrupt status is continually 01111111b, with bit 7 never being set.  Seek works fine, with interrupt status bit 7 being set when seek completes.  This implies that the read from the floppy isn't being interrpreted by the conttroller for some unknown reason.  I swapped out the FDC chip with no change.

### 04-11-2024

No read data reaching FDC chip.  U7 74LS279 tested bad and was swapped with my other DISK1.  Read data is now reaching U19b but no RD DATA out.

### 08-11-2024

Faulty OP-Amp at U6, a 4136.  Replaced with one from old DISK1 and it Reads Id successfully.  Read Data fails with ST0 D7-6 of 01, abnormal termination.

Set EOT on Read Data to 1 instead of 26, but not change.

### 10-11-2024

I think I've been using DS1 and DS0 as binary, but I think they're
selection bit for 2 or 1.  Made SEND_CMD use FD_U and added
U command to set the disk unit...  No, they are binary, 00 is the first drive. 01 is the second.

Added an FD_EOT field so the EOT value can be varied at runtime.

### 11-11-2024

I'm moving R35, the VCO potentiometer to see if it affects things.  It was outputting 2.308MHz, which seems a bit high for the docs stated "Approximately 2MHz output".

MovIng down to 2.125Mhz, the status register 1 for a read data changed from MA to DE (Data Error), and SR2 went from MD to DD.  2.149MHz went back to MA,MD.  But no success.

### 26-11-2024

Switched back to 171D setup for 8" floppy, and connected to two Shugart 800.  Modified the 800s from the Altos ACS8000 setup to DISK1 recommended setup, the only difference being the HL jumper, which is removed for DISK1, present for Altos.  Seek works now.

I modified the code to store more in the FD_ data to allow it to be copied as a block of 8 bytes for the read command that uses Unit, Cyl, Head, Sector, SecSiz, EOT, GPL and DTL.

With the 800s, the read still returns either MA or ND.  What is the matter!?!

### 09-12-2024

A big change.

I realised the CPU-Z on-board RAM isn't DMA enabled.  And it's also an either/or with the CPU-Z on-board 
RAM and RAM17 card.  So let's move the ROM to the System Support 1 ROM slot, which can coexist with RAM17 as RAM.  
I set SS1 ROM to address 0000-07FF, and disabled RAM17 block 0000-3FFF.

This took around 4 days to get working because the change just wouldn't work.  The ROM code couldn't be found!  
Chip Select for the ROM was traced as looking very suspect, and this was traced back to U33 74LS10 bad on 
System Support 1 preventing chip select on ROM.  Beware - XGPRO Logic tester passed it as a good chip!

### 10-12-2024

The code runs fine, but Ready isn't being detected.  I swapped the USB HxC with a Gotek but that's the same.
Swapped the FDC chip - same.  Swapped 50pin IDC - same.  34pin - same.  Oh - U16 & U32 are missing - Ready is detected.
Now, status isn't indicating ready on READ ID command.  The results are all zero (this used to work).
And if I send a second READ ID, it hangs.  Wait states incorrectly set?  I lowered the CPU speed to 2MHz 
with no change.

### 12-12-2024

Partial Success.  I got a sector transferred through DMA.  I used a Kaypro floppy image which is recorded at 500kbps at 360RPM.
I discovered it when using a logic analyser showing the RD and WR pins of the 8272 and saw bursts of reads.  The FDC must be reset 
after each attempt, or the DMA address keeps incrementing.  I still get an MA error or sometimes an EN end of cylinder error.

![First DMA](/assets/2024-12-12_23_44_42-DSView.jpg "First DMA")

The setting of the potentionmeter R35 has a bearing on the readability.  To get it to read I went much further anti-clockwise than the factory setting that was painted into position.

### 19-12-2024

Added a Zero memory command.
The ROM is FULL!  I removed the Binary Echo command and compacted the help output to save space.