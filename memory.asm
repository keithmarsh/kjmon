        INCLUDE "global.asm"
        DEFC    ROMSTA      =   0000H
        DEFC    ROMEND      =   07FFH     
        DEFC    RAMSTA      =   4000H               ; RAM AREA TO COPY THIS ROM TO
        DEFC    RAMEND      =   7FFFH
        DEFC    RAMRSV      =   0AAH                ; Adjust so LOG = 0FFF
        DEFC    RAMWORK     =   (RAMEND - RAMRSV)
        SECTION ROM
        ORG     ROMSTA
        SECTION DATA_RW
        ORG     RAMWORK