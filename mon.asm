; ROM Monitor KJMon for debugging Compupro cards
; COMPUPRO CPU-Z
; COMPUPRO RAM17
; COMPUPRO DISK1
; COMPUPRO Sytem Support 1
;
; On boot, this monitor
; * inits uart on SS1 to 9600
; * prints welcome
; * copies itself to 8000H
;
; USE RAM17 AREA AS IT WORKS WITH DMA
; CPU-Z ONBOARD MEM DOES NOT
;
    SECTION	ROM
            INCLUDE	"global.asm"
            ;
            ; Constants
            ;
DEBUG       EQU	1                   ; Set this to get values written and read to/from DISK1
B19200      EQU 01111111b
B9600       EQU	01111110b
B1200       EQU	01110111b
B300        EQU	01110101b
STOPS_2     EQU 11000000b
STOPS_1_5   EQU 10000000b
STOPS_1     EQU 01000000b
PARITY_ODD  EQU 00010000b
PARITY_EVEN EQU 00110000b
PARITY_NONE EQU 00000000b
BITS_8      EQU 00001100b
BITS_7      EQU 00001000b
ASYNC_16X   EQU 00000010b
BL          EQU	' '                 ; Blank
BEL         EQU	07H
BS          EQU	08H
LF          EQU	0AH
CR          EQU	0DH
ESC         EQU	1BH
RUBOUT      EQU	7FH
; SYSTEM SUPPORT 1
GBSS        EQU	50h                 ; System Support starting port
GBUD        EQU	GBSS + 0CH          ; Uart data port
GBUS        EQU	GBSS + 0DH          ; Uart status port
GBUM        EQU	GBSS + 0EH          ; Uart modem port
GBUC        EQU	GBSS + 0FH          ; Uart command port
SSDAV       EQU	00000010b           ; System Support Data Available
SSTBMT      EQU	00000001b           ; System Support Transmit Buffer Empty

COLDBOOT:
            LD	SP, STACK
            JP	STARTUP
BOOTEND:
RST_08      EQU 0008H
RST_10      EQU	0010H
RST_28      EQU	0028H
RST_38      EQU	0038H
            ;
            ; Maintain a checksum in C
            ;
            DS  (RST_08 - BOOTEND)
RST08:      ADD A, C
            LD  C, A
            RET
RST08END:
            ;
            ; A contains an ASCII hex character.  Validate it
            ; and leave the answer in A low 4 bits.  Carry set
            ; if invalid, clear if valid.
            ;
            DS	(RST_10 - RST08END), 0FFH
RST10:      CALL    CHIN
            SUB	'0'
            RET C
            CP	10
            JR	C, HEXOK
            SUB	07
            CP	10
            RET C
            CP	16
            JR	C, HEXOK
HEXZZ:      SCF
            RET
HEXOK:      CCF
            RET
RST10END:
;
;
;
            ; PRINT A NULL TERMINATE STRING FOLLOWING AN RST 28H
            ; CODE ABOVE MUST NOT EXCEED 27H

            DS	(RST_28 - RST10END), 0FFH
            EX	(SP), HL
RST28:      LD	A, (HL)
            INC	HL
            OR	A
            JR	Z, PRS2
            CALL	CHOUT
            JR	RST28
PRS2:       EX	(SP), HL
            RET
RST28END:
            DS	(RST_38 - RST28END), 0FFH
            ; HALT IF 0xFF IS ACCIDENTALY EXECUTED
            ; AT RST38
RST38:      HALT

            ;SECTION MONITOR
            ; STARTUP
STARTUP:
SAFESP:     LD	HL, ST_UNFL         ; WRITE RST38 ADDRESS SO THAT HALTS WHEN OVERFLOW
            LD	DE, RST38
            LD	(HL), E
            INC	HL
            LD	(HL), D
            LD	IX, LOG             ; POINTER TO LOG SETTING
UART:       LD	HL, UARTSPD         ; INIT TERMINAL
            LD	A, B19200
            LD	(HL), A
UARTINI:    ; SYSTEM SUPPORT 1 UART ROUTINES
            ; Uses: A
            LD	A, STOPS_2 | PARITY_EVEN | BITS_7 | ASYNC_16X       ; Async, 16x, 8 bits, no parity, even, 2 stops
            OUT	(GBUM), A           ; Set up mode register 1
            LD	A, (UARTSPD)
            OUT	(GBUM), A           ; Set up mode register 2
            LD	A, 00100111b        ; Trans. on, dtr low, rec. on, no break, no reset, rts low
            OUT	(GBUC), A           ; Set up command port

WELCOME:
            LD	HL, S_WELC          ; PRINT WELCOME MESSAGE
            CALL	STOUT
COPYDRAM:
            LD	HL, DROM            ; COPY DSKROM TO DSKRAM
            LD	DE, DRAM
            LD	BC, DROML           ; BYTE COUNT
            LDIR
            JP	PARSE

            ; Input serial to A
            ; Uses: F
            ; Returns A
CHIN:       IN	A, (GBUS)           ; GET UART STATUS
            AND	SSDAV
            JR	Z, CHIN
            IN	A, (GBUD)
            AND	07FH                ; MY SILENT 700 SENDS CR AS 8D
            RET

CHOUT:      ; Output A to serial
            ; Uses: None
            PUSH	AF
CHOUT1:     IN	A, (GBUS)           ; Get uart status
            AND	SSTBMT              ; Test if buffer empty
            JR	Z, CHOUT1           ; Loop if not
            POP	AF
            OUT	(GBUD), A
            RET
            ; NULL TERMINATED STRING output to serial
            ; HL: Address of string start
STOUT:      PUSH	AF
STOUT1:     IN	A, (GBUS)           ; GET UART STATUS
            AND	SSTBMT
            JR	Z, STOUT1           ; WAIT IF NOT READY TO WRITE
            LD	A, (HL)             ; GET CHAR
            INC	HL                  ; MOVE TO NEXT
            OR	A                   ; NULL?
            JR	Z, STOUT2           ; LEAVE IF YES
            OUT	(GBUD), A           ; SEND CHAR
            JR	STOUT1              ; NEXT CHAR
STOUT2:     POP	AF
            RET

; READ A LINE INTO BUFFER 
; USES B, C, DE
INLINE:     RST	28H
            DB	">", 0
            LD	DE, BUFFER          ; POINT TO FIRST CHAR IN BUFFER
            XOR	A
            LD	(DE), A             ; SET CHAR0 TO 0
            LD	B, A                ; SET LENGTH TO 0
NEXTCH:     CALL	CHIN            ; GET  CHARACTER IN A
            CP	BS
            JR	Z, CH_BS            ; BACKSPACE
            CP	RUBOUT
            JR	Z, CH_BS            ; RUBOUT (DEL)
            CP	CR
            JR	Z, CH_CR            ; CARRIAGE RETURN
            CP	ESC
            JR	Z, INLINE
            LD	C, A                ; SAVE CHAR IN C
            LD	A, B                ; GET BUFLEN
            CP	BUFSIZ              ; COMPARE TO MAX
            JR	NC, REJECT          ; BUFFER OVERFLOW?
            LD	A, C                ; RESTORE A
            LD	(DE), A             ; STORE THE CHAR IN THE BUFFER
            INC	DE                  ; MOVE TO NEXT SPACE
            INC	B                   ; INCREMENT LENGTH
            CALL	CHOUT               ; DISPLAY CHARACTER
            JR	NEXTCH              ; GET NEXT
CH_BS:      LD	A, B
            OR	A                   ; IS LENGTH 0
            JP	Z, REJECT           ; DON'T BACKSPACE IF SO
            DEC	DE
            DEC	B
            LD	A, BS
            CALL	CHOUT
            JR	NEXTCH
REJECT:     LD	A, BEL              ; PING THE BELL AND WAIT FOR NEXY CHAR
            CALL	CHOUT
            JR	NEXTCH
CH_CR:      LD	A, B
            CP	0                   ; NO CHARS?
            JR	Z, NEXTCH           ; DON'T RETURN, WAIT FOR MORE
            XOR	A
            LD	(DE), A             ; PUT A NUL AT THE END
            LD	DE, BUFLEN          ; SAVE THE BUFFER LENGTH
            LD	A, B                ; PUT LEN IN A (03/11/24 FIX)
            LD	(DE), A
NL2:        RST	28H                 ; OUTPUT CR,LF
            DB	CR, LF, 0
            RET
; subroutine to read hex humber off line and convert to binary
; DE POINTS TO CHAR IN COMMAND LINE TO READ
; LEAVES HL POINTING TO NUM - DIGITS IN VALUE READ
NEXNUM:     LD	A, (DE)             ; LINE POSITION IN DE
            CP	BL
            INC	DE                  ; DOESN'T AFFECT FLAGS
            JR	Z, NEXNUM           ; SKIP OVER SPACES
            DEC	DE
            XOR	A
            LD	HL, NUM             ; ADDRESS OF DIGITS IN NUMBER
            LD	(HL), A             ; RESET COUNT TO 0
            INC	HL
            LD	(HL), A             ; RESET HI VAL
            INC	HL
            LD	(HL), A             ; RESET LO VAL
NN1:        LD	A, (DE)             ; READ DIGIT
            DEC	HL
            DEC	HL                  ; HL BACK TO NUM
            SUB	'0'
            RET	M                   ; BAIL IF MINUS
            CP	10
            JR	C, NN2              ; IN RANGE, SAVE IT
            SUB	07
            CP	10
            RET	M                   ; OUT OF RANGE
            CP	16
            RET	P                   ; OUT OF RANGE
NN2:        INC	DE                  ; NEXT DIGIT
            INC	(HL)                ; INCREMENT DIGIT COUNT
            INC	HL
            RLD                     ; SAVE BINARY VALUE IN NUM+1
            INC	HL
            RLD
            JR	NN1
; LINE INTERPRETER
PARSE:      CALL	INLINE          ; PROMPT AND READ IN RESPONSE LINE
            LD	DE, BUFFER
            LD	BC, CMD
            LD	A, (DE)
            CP	BL                  ; FIRST CHAR A SPACE
            JR	NZ, PARSE1          ; NO PROCEED
            LD	A, (BC)             ; YES, CHECK IF STEP COMMAND
            CP	'S'
            JR	NZ, PARSE           ; NOT IN SINGLE STEP MODE
PARSE1:     LD	(BC), A             ; SAVE COMMAND CHAR AT ARGS
            INC	BC                  ; SET BC TO ARGC
            INC	DE                  ; NEXT BUFFER CHAR
            XOR	A
            LD	(BC), A             ; SET ARGC TO 0
PLOOP:      INC	BC                  ; SET BC TO ARG1
            CALL	NEXNUM          ; GET ARGUMENT
            LD	A, (HL)             ; LOAD DIGITS READ
            OR	A
            JR	Z, PEND             ; DONE WHEN NO DIGITS
            INC	HL                  ; MOVE TO VALUE READ
            LD	A, (HL)             ; COPY VALUE
            LD	(BC), A             ; TO ARGn
            INC	HL
            INC	BC
            LD	A, (HL)
            LD	(BC), A
            LD	HL, ARGC
            INC	(HL)                ; INC ARGC
            JR	PLOOP
PEND:       LD	BC, (CMD)           ; GET THE COMMAND IN C
            LD	HL, CTAB
PEND1:      LD	A, (HL)             ; GET A COMMAND FROM THE TABLE
            OR	A
            JR	Z, PARSE            ; LAST COMMAND OF TABLE
            INC	HL                  ; COMMAND ADDRESS
            CP	C                   ; FOUND A COMMAND?
            JR	Z, PEND2            ; YES
            INC	HL                  ; NO - SKIP ADDRESS
            INC	HL
            JR	PEND1               ; TRY AGAIN
PEND2:      LD	E, (HL)             ; GET COMMAND ROUTINE ADDRESS IN DE
            INC	HL
            LD	D, (HL)
            LD	HL, PARSE           ; SET PARSE ON STACK
            PUSH	HL
            EX	DE, HL              ; SWAP WITH DE
            JP	(HL)                ; JUMP TO COMMAND ROUTINE
            ;
            ; USER COMMAND
            ; MODIFY RAM
            ;
CMD_MODIFY: ld	hl, (ARG1)          ; ADDRESS TO BE DISPLAYED
mod1:       call	TAB3            ; DISPLAY ADDRESS
            ld	a, (hl)
            call	B2HEX           ; DISPLAY CONTENTS
            call	INLINE          ; PROMPT AND READ
            ld	de, BUFFER
            ld	b, 0                ; ZERO TALLY
mod2:       push	hl
            call	NEXNUM          ; READ HEX VALUE
            ld	a, (hl)             ; DIGITS READ
            or	a
            jr	z, MOD3             ; NOT A NUMBER
            inc	hl
            ld	a, (hl)
            pop	hl
            ld	(hl), a             ; SAVE NEW VALUE IN MEMORY
            inc	b                   ; GET NEXT
            inc	hl                  ; NEXT LOCATION
            jr	mod2
MOD3:       pop	hl
            ld	a, (de)             ; NEXT CHAR
            cp	'.'                 ; EXIT TO COMMAND MODE IF '.'
            ret	z
            ld	a, b
            or	a
            jr	nz, MOD4            ; FINISH LINE GET NEXT BYTE
            inc	hl
MOD4:       jr	mod1
            ;
            ; USER COMMAND
            ; copy, arguments: from, to, length
            ;
CMD_COPY:   ld	hl, (ARG1)
            ld	de, (ARG2)
            ld	bc, (ARG3)
            ldir
            ret
            ;
            ; USER COMMAND
            ; ZERO MEMORY from ARG1 up to ARG2 with 00 or ARG3
            ;
CMD_ZERO:   ld  hl, (ARG1)
            ld  de, (ARG2)
            dec de                  ; one less so up to but not including
            ld  a, (ARGC)
            ld  b,0
            cp  2
            ret c                   ; Bail < 2 args
            jr  z, CMD_ZEROB
            cp  3
            jr  z, CMD_ZERO3
            ret nc                  ; Bail > 3 args
CMD_ZERO3:  ld  a, (ARG3)
            ld  b, a
CMD_ZEROB:  push  hl
            sbc hl, de              ; Subtract end from start. if 0, done
            pop hl
            ret z
            ld  (hl), b
            inc hl
            jr  CMD_ZEROB
            ;
            ; USER COMMAND
            ; EXECUTE PROGRAM - ARG IS START ADDRESS
            ;
CMD_EXEC:   ld	a, (ARGC)           ; GET ARG COUNT
            CP	1
            RET	NZ
            LD	HL, (ARG1)          ; GET ARG1 TO HL 
            JP	(HL)
            ; USER COMMAND
	          ; SET THE DMA ADDRESS
            ;
CMD_SETDMA: LD	A, (ARGC)           ; GET ARG COUNT
            CP	1                   ; IS IT 1?
            RET	NZ                  ; BAIL IF NOT
            LD	DE, (ARG1)
            LD  HL, NDMA+1
            LD  (HL), D
            INC HL
            LD  (HL), E
            JP	DODMA
            ;
            ; USER COMMAND
            ; DISK UNIT COMMAND - arg is disk unit
            ;
CMD_UNIT:   LD	A, (ARGC)           ; GET ARG COUNT
            CP	1                   ; IS IT 1?
            RET	NZ                  ; BAIL IF NOT
            LD	DE, (ARG1)          ; Get the argument
            LD  A, E                ; in A
            LD  (FD_U), A           ; Store it in FD_U
            RET
            ;
            ; USER COMMAND
            ; SEEK COMMAND - arg is track
            ;
CMD_SEEK:   LD	A, (ARGC)           ; GET ARG COUNT
            CP	1                   ; IS IT 1?  
            RET	NZ                  ; BAIL IF NOT
            LD	HL, (ARG1)          ; GET ARG1 TO HL
            LD	A, L
            JP	DOSEEK
            ;
            ; USER COMMAND NOT DONE
            ;
CMD_TODO:   RST	28H                 ; DISPLAY ERROR
            DB	"TODO", CR,LF,0
            RET
CMD_FMT:    ;
            ; USER COMMAND
            ; FORMAT COMMAND
            ;
            JP	DOFMT
            ; USER COMMAND
            ; SET TRACKS TO tt and N to nn
CMD_TRACKS: LD	A, (ARGC)           ; GET ARG COUNT
            CP	3                   ; IS IT 3?
            JNZ DISK_INFO           ; LOG DISK CONFIG
            LD	HL, (ARG1)          ; ARG1 IN A
            LD  DE, FD_T
            LD	(DE), H             ; SAVE TRACKS
            LD  DE, FD_N
            LD  (DE), L             ; SAVE N
            LD  HL, (ARG2)
            INC DE
            LD  (DE), H             ; SAVE SECTORS
            LD  DE, FD_DEN
            LD  (DE), L             ; SAVE DENSITY
            LD  HL,(ARG3)
            LD DE, FD_GP
            LD  (DE),H              ; SAVE GAP1
            INC DE
            LD  (DE),L              ; SAVE GAP3
            JP  DISK_INIT           ; CALL THE FLOPPY UPDATE ROUTINE
            ; USER COMMAND
            ; LOG LEVEL ll
CMD_LOGLEVEL:
            LD	A, (ARGC)           ; GET ARG COUNT
            CP	1                   ; IS IT 1?
            RET	NZ                  ; BAIL IF NOT
            LD	A, (ARG1)           ; ARG1 IN A
            LD  (IX),A
            RET
            ; USER COMMAND
            ; READ SECTOR tt ss TO DMA MEMORY 
CMD_READ:   LD	A, (ARGC)           ; GET ARG COUNT
            CP	2                   ; IS IT 2?
            JR  NZ,CMD_READT        ; TRACK IF NOT
            LD  A, F_RDAT           ; READ DATA COMMAND
            LD  (RDAT), A           ; RELOAD IT
            LD  A, (ARG2)           ; ARG2 IN A
            JR  CMD_READ2
CMD_READT:  LD  A, (ARGC)           ; RELOAD ARG COUNT
            CP  1                   ; HOW ABOUT 1?
            RET NZ                  ; BAIL IF NOT 1
            LD  A, F_RTRK           ; READ TRACK COMMAND
            LD  (RDAT), A           ; RELOAD IT
            LD  A, 1                ; SECTOR 1
CMD_READ2:  LD  (FD_R), A           ; SAVE SECTOR
            LD	A, (ARG1)           ; ARG1 IN A
            LD	(FD_C), A           ; SAVE TRACK
            JP  DOREAD              ; CALL THE READ FLOPPY ROUTINE
            ; USER COMMAND
            ; WRITE SECTOR tt ss FROM DMA MEMORY
CMD_WRITE:  LD	A, (ARGC)           ; GET ARG COUNT
            CP	2                   ; IS IT 2?
            RET	NZ                  ; BAIL IF NOT
            LD	A, (ARG1)           ; ARG1 IN A
            LD	(RDAT+2), A         ; SAVE TRACK
            LD  A, (ARG2)           ; ARG2 IN A
            LD  (RDAT+4), A         ; SAVE SECTOR
            JP  DOWRITE             ; CALL THE WRITE FLOPPY ROUTINE
            ;
            ;
            ; USER COMMAND
            ; TABULATE RAM CONTENTS
            ;
CMD_LIST:   LD	HL, S_TAB
            CALL	STOUT
            LD	HL, (ARG1)          ; SETUP START ADDRESS
            LD	DE, (ARG2)          ; TEST FOR COMPLETION
            LD	A, H
            CALL	B2HEX
            LD	A, L
            CALL	B2HEX
            LD	A, BL
            CALL	CHOUT
            LD	A, D
            CALL	B2HEX
            LD	A, E
            CALL	B2HEX
            LD	HL, S_CRLF
            CALL	STOUT
            LD	HL, (ARG1)          ; SETUP START ADDRESS
TAB1:       LD	DE, (ARG2)          ; TEST FOR COMPLETION            
            PUSH	HL
            OR	A
            SBC	HL, DE
            POP	HL
            JR	C, TAB10            ; NOT COMPLETED
            RST	28H                 ; FINISHED IDSPLAYING NOW PROMPT
            DB	'.', CR, LF, 00     ; RETURN TO COMMAND MODE
            RET
TAB10:      CALL	TAB3            ; DISPLAY ADDRESS
            LD	B, 08H              ; 8 LOCATIONS ON A LINE
TAB1A:      LD	A, (HL)
            CALL	B2HEX           ; DISPLAY LOCATION CONTENT
            INC	HL                  ; NEXT LOCATION
            CALL	SPACE           ; SPACE SEPERATES VALUES
            DJNZ	TAB1A           ; 8 LOCATIONS
            CALL	CRLF
            JR	TAB1
            ; DISPLAY ADDRESS IN HL IN HEX
TAB3:       LD	A, H
            CALL	B2HEX           ; HIGH ORDER ADDRESS
            LD	A, L
            CALL	B2HEX           ; LOW ORDER ADDRESS
            JR	SPACE
            ; DISPLAY A SPACE
SPACE:      LD	A, BL
            JR	JCHOUT
            ; CARRIAGE RETURN LINE FEED
CRLF:       LD	A, CR
            CALL	CHOUT
            LD	A, LF
            JR	JCHOUT
            ; PRINT BYTE IN A IN BINARY
            ; USES BC, HL
PRINTA:     LD	HL, BUFFER
            CALL	A2BIN
            LD	(HL), CR
            INC	HL
            LD	(HL), LF
            INC	HL
            LD	(HL), 0
            LD	HL, BUFFER
            JP	STOUT
            ; PUT BYTE IN A TO BINARY NUL TERM STRING AT HL. 
A2BIN:      LD	C, A
            PUSH	AF
            PUSH	BC
            LD	B, 8
B2BIN1:     LD	A, '0'
            BIT	7, C
            JR	Z, B2BIN2
            INC	A
B2BIN2:     LD	(HL), A
            SLA	C
            INC	HL
            DJNZ	B2BIN1
            LD	(HL), B
            POP	BC
            POP	AF
            RET
            ; BYTE IN A TO HEX DISPLAY
B2HEX:      PUSH	AF
            RRA
            RRA
            RRA
            RRA
            CALL	B2HEX1          ; DISPLAY LEFT NIBBLE
            POP	AF                  ; RESTORE BYTE
B2HEX1:     AND	0FH                 ; RIGHT NIBBLE ONLY
            ADD	A, '0'              ; MAKE ASCII
            CP	'9' + 1             ; LETTER?
            JR	C, JCHOUT           ; NO, DISPLAY IT
            ADD	A, 07               ; YES, MAKE 'A'+
JCHOUT:     JP	CHOUT
            ;
            ; USER COMMAND
            ; INTEL HEX
            ;
CMD_HEX:    ; B - Count
            ; C - Checksum
            ; DE - Target Address
            ; HL - Buffer
            ;
            RST 28H
            DB  "HEX",CR,LF,0
            LD  A, 0
            LD  (HEXEXIT), A
            LD  (HEXERRS), A
            ; READ UNTIL :
PAST_COLON: LD	HL, BUFFER
            CALL	CHIN
            CP	':'
            JR	NZ, PAST_COLON
            ; RESET CHECKSUM
            LD	C, 0
            ; COUNT IN B
            CALL    RDHEXBYTE
            LD	B, A
            RST 08H
            ; ADDR IN DE
            CALL    RDHEXBYTE
            LD	D, A
            RST 08H
            CALL    RDHEXBYTE
            LD	E, A
            RST 08H
            ; TYPE IN A
            CALL    RDHEXBYTE
            PUSH	AF
            RST 08H
            POP	AF
            ; IF NOT RECORD TYPE 0 TRY NEXT TYPE
            CP	0
            JR	NZ, IS_IT_END
            ; TRANSFER THE DATA
DATA_BYTE:
            CALL	RDHEXBYTE       ; Answer left in (HL) as well as A
            RST 08H
            PUSH    BC
            LDI                     ; Copy (HL++) to (DE++)
            POP     BC              ; We count in B
            DJNZ    DATA_BYTE       ; Load all the data
            JR	CHECK_SUM
            ; IF NOT RECORD TYPE 1 GOTO ERROR
IS_IT_END:  CP	1
            CALL	NZ, RDHEXERR
            ; SET EXIT FLAG
            LD	HL, HEXEXIT
            LD	(HL), 1
            ; VALIDATE CHECKSUM
CHECK_SUM:  CALL	RDHEXBYTE
            ADD	A, C                ; Checksum Added to correct checksum should make zero
            JR  Z, HEXLOOP          ; If all ok, next record
            LD  HL, HEXERRS         ; If not, 
            INC (HL)                ; increment error count, but continue
HEXLOOP:    ; Loop unless exit flagf set
            LD	A, (HEXEXIT)
            OR	A
            JR	Z, PAST_COLON
HEXDONE:    ; Check error count
            LD  A, (HEXERRS)
            CP	0
            RET Z
            RST 28H
            DB  "Errors in HEX",CR,LF,0
            RET
RDHEXBYTE:  ;
            ; Read a hex pair into A and also (HL)
            ;
            RST 10H
            JR	C, RDHEXERR
            RLD
            RST 10H
            JR  C, RDHEXERR
            RLD
            LD  A,  (HL)            ; Copy the answer from (HL) into A too
            RET
RDHEXERR:   ; Mark an error and continue
            CALL B2HEX
            LD	HL, HEXERRS
            INC	(HL)
            DEC SP                  ; Ignore return, jump to end of loop
            JR  HEXLOOP
CMD_HELP:   ;
            ; User Command
            ; HELP
            ;
            RST 28H
            DB	"List   L AAAA BBBB",CR,LF
            DB	"Modify M AAAA",CR,LF
            DB	"Copy   C AAAA BBBB NN",CR,LF
            DB	"Exec   E AAAA",CR,LF
            DB	"Zero   Z AAAA BBBB [CC]",CR,LF
            DB	"Hex    X",CR,LF
            DB  "Init   I",CR,LF
            DB	"DMA    A AAAA",CR,LF
            DB  "Unit   U AA",CR,LF
            DB  "Status D",CR,LF
            DB	"Seek   S TT",CR,LF
            DB	"Tracks T [TTNN SSDD GGFF]",CR,LF
            DB	"Format F",CR,LF
            DB	"Read   R TT [SS]",CR,LF
            DB	"Write  W TT SS",CR,LF
            DB  "Where  H",CR,LF
            DB  "Log    O AA",CR,LF,0
            RET
            

; STATIC KJMON DATA
            INCLUDE	"version.inc"
S_TAB:      DB	"TABULATE ", 0
S_BECO:     DB	"ESC"
S_CRLF:     DB	CR, LF, 0
S_PRMP:     DB	".", 0
            ; COMMAND CONTROL TABLE
CTAB:       DB	'L'                 ; L AAAA BBBB
            DW	CMD_LIST            ;   List from AAAA to BBBB or till ESC pressed
            DB	'M'                 ; M AAAA
            DW	CMD_MODIFY          ;   Modify AAAA till . entered
            DB	'C'                 ; C AAAA BBBB NN
            DW	CMD_COPY            ;   Copy from AAAA to BBBB for NN bytes
            DB	'E'                 ; E AAAA
            DW	CMD_EXEC            ;   Execute from AAAA
            DB	'Z'                 ; Z AAAA BBBB CC
            DW	CMD_ZERO            ;   Zero from AAAA up to BBBB with 00 or CC
            DB	'X'                 ; X AAAA
            DW	CMD_HEX             ;   Load Intel Hex format to AAAA
            DB	'A'                 ; A AAAA
            DW	CMD_SETDMA          ;   Set DMA to AAAA
            DB  'D'                 ; D
            DW  CMD_DSKSTATUS       ;   Disk Status
            DB	'I'                 ; I
            DW	CMD_DISKINIT        ;   Initialize Disk System
            DB	'U'                 ; U nn
            DW	CMD_UNIT            ;   Set disk unit to nn
            DB	'S'                 ; S nn
            DW	CMD_SEEK            ;   Seek to track nn
            DB	'T'                 ; T ttnn ssdd ggff
            DW	CMD_TRACKS          ;   tt:tracks nn:N ss:sectors dd:0=FM,1=MFM gg:gap ff:gap fmt
            DB	'F'                 ; F
            DW	CMD_FMT             ;   FORMAT DISK
            DB	'R'                 ; R NN MM
            DW	CMD_READ            ;   Read a sector of data from track NN sector MM to DMA memory
            DB	'W'                 ; W NN MM
            DW	CMD_WRITE           ;   Write a sector of data to track NN sector MM from DMA memory
            DB  'H'                 ; H
            DW  CMD_READID          ;   Where's the head - read id of next sector under head
            DB  'O'
            DW  CMD_LOGLEVEL        ; Log Level b0
            DB  '?'
            DW  CMD_HELP
            DB	0                   ; END OF TABLE
ENDMON:
;
;
;   END OF MONITOR
;
;
DISK1:
;
; DISK1 CONSTANTS
;
SSSD_8      EQU 8
SSSD_525    EQU 525
; You can override this at compile time with -DFLPY_SIZE=8|525
IFNDEF FLPY_SIZE
FLPY_SIZE   EQU SSSD_8
ENDIF

IF FLPY_SIZE = SSSD_8
  FDPORT      EQU	0C0H                ;Base port address for Controller
  SRT         EQU	16 - 8              ; STEP RATE TIMER 8MS
  INIT_TRK    EQU 77
  INIT_SEC    EQU 26
  INIT_GAP    EQU 7                   ; See comments above LOGPAD:
  INIT_GAPF   EQU 27                  ; https://www.hermannseib.com/documents/floppy.pdf
ELSE
  FDPORT      EQU	0CCH                ;Base port address for minifloppy Controller
  SRT         EQU	16 - 3              ; STEP RATE TIMER 3MS
  INIT_TRK    EQU 40
  INIT_SEC    EQU 18
  INIT_GAP    EQU 9
  INIT_GAPF   EQU 29
ENDIF

FDCS        EQU	FDPORT ;Status register
FDCD        EQU	FDPORT + 1 ;Data register
DMA         EQU	FDPORT + 2 ;Dma address (when write)
INTS        EQU	FDPORT + 2 ;Status Register (when read)

F_RTRK      EQU	02                  ; Read track
F_SPEC      EQU	03                  ; Specify
F_SDST      EQU	04                  ; SENSE Drive status
F_WDAT      EQU	05                  ; WRITE DATA
F_RDAT      EQU	06                  ; Read data
F_RECA      EQU	07                  ; recalibrate
F_SIST      EQU	08                  ; SENSE INTERRUPT status
F_RDID      EQU	0AH                 ; READ ID
F_FMT       EQU	0DH                 ; FORMAT
F_SEEK      EQU	0FH                 ; Seek

SRBits      EQU	11000000B   ; Bits indicating read/write readiness
SRRd        EQU	11000000B   ; Data Register Ready for Read
SRWrt       EQU	10000000B   ; Data Register Ready for Write
SRF1Bs      EQU	00000010B   ; Status Register FDD1 Busy

HUT         EQU	240 / 16            ; HEAD UNLOAD TIMER 240MS (is this too low?)
HLTT        EQU	(35 + 1) / 2        ; HEAD LOAD TIMER 35MS
NND         EQU	0                   ; NON NON-DMA = USE DMA
ND          EQU	1                   ; NON-DMA = USE INTERRUPTS

CMDMT       EQU	10000000B           ; SET MULTITRACK
CMDMFM      EQU	01000000B           ; SET MFM/DD
CMDSK       EQU	00100000B           ; SET SKIP DELETED DATA
CMDMSK      EQU	00011111B           ; MASK FOR THE COMMAND BITS

LOGRD       EQU	0                   ; BIT TO SET TO LOG READS
LOGWR       EQU	1                   ; BIT TO SET TO LOG WRITES
LOGST       EQU	2                   ; BIT TO SET TO LOG STATUS
LOGINT      EQU 3                   ; BIT TO SET TO LOG INTERRUPT STATUS

;
; DISK CODE
;
CMD_DISKINIT:
            LD	A, 3                ; LOG READ AND WRITES
            LD	(LOG), A
            LD	A, INIT_TRK
            LD	(FD_T), A           ; NUMBER OF TRACKS
            LD  A, INIT_SEC
            LD  (FD_SC), A          ; Sectors/track
            LD  A, INIT_GAP
            LD  (FD_GP), A          ; Gap
            LD  A, INIT_GAPF
            LD  (FD_GF), A          ; Gap Format (Gap3?)
            LD  A, 128
            LD  (FD_DTL), A         ; DTL
            LD  A, 2 
            LD  (FD_EOT), A         ; EOT
            LD	A, 0
            LD	(FD_H), A           ; 1 head
            LD	(FD_DEN), A         ; FM
            LD	(FD_N), A           ; N=0
            LD	(FD_U), A           ; DS1

            ;
            ; CALL HERE IF ANY FD_N OR FD_DEN ARE MODIFIED
            ;
DISK_INIT:  CALL	DODMA
            CALL	DOSPECD
            CALL	DORECA
            CALL	DOSIST
DISK_INFO:  RST 28H
            DB "DMA=",0
            LD  A, (NDMA+1)
            CALL B2HEX
            LD  A, (NDMA+2)
            CALL    B2HEX
            RST 28H
            DB  "H U=", 0
            LD  A,(FD_U)
            CALL B2HEX
            RST 28H
            DB  " H=", 0
            LD  A,(FD_H)
            CALL B2HEX
            RST 28H
            DB  ", T=", 0
            LD  A,(FD_T)
            CALL B2HEX
            RST 28H
            DB  "H N=", 0
            LD  A,(FD_N)
            CALL B2HEX
            RST 28H
            DB  ", SC=", 0
            LD  A,(FD_SC)
            CALL B2HEX
            RST 28H
            DB  "H DEN=", 0
            LD  A,(FD_DEN)
            CALL B2HEX
            RST 28H
            DB  ", GP=", 0
            LD  A,(FD_GP)
            CALL B2HEX
            RST 28H
            DB  "H GF=", 0
            LD  A,(FD_GF)
            CALL B2HEX
            RST 28H
            DB  "H",CR, LF, 0

            RET
            ; SEND 3 BYTES, MOST SIGNIFICANT BYTE FIRST
DODMA:      LD	DE, NDMA
            LD	B, NDMA_L
DMA1:       LD	A, (DE)
            OUT	(DMA), A
            INC	DE
            DJNZ	DMA1
            RET
            ; SPECIFY WITH DMA
DOSPECD:    LD	B, SPEC_L
            LD	DE, SPDMA
            CALL	SEND_CBUF
            RET
            ; SPECIFY WITH INTERRUPTS
DOSPECI:    LD	B, SPEC_L
            LD	DE, SPINT
            CALL	SEND_CBUF
            RET
            ; RECALIBRATE
DORECA:     LD	B, RECAL_L
            LD	DE, RECAL
            CALL	SEND_CBUF
            CALL	ISDONE
            RET
            ; SEEK - cylinder in a
            ; Uses B, DE, HL
DOSEEK:     LD	(SEEK + 2), A
            LD	B, SEEK_L
            LD	DE, SEEK
            CALL	SEND_CBUF
            CALL	ISDONE
            ; SENSE INTERRUPT STATUS
DOSIST:     LD	B, SIST_L
            LD	DE, SIST
            CALL	SEND_CBUF
            LD	B, RSIST
            CALL	RDRSLT
            RET
            ; SENSE DRIVE STATUS
CMD_DSKSTATUS:
DOSDST:     LD	B, SDST_L
            LD	DE, SDST
            CALL	SEND_CBUF
            LD	B, RSDST
            CALL	RDRSLT
            RET
            ; READ DATA.  
            ; CMD, U, C, H, R, N, EOT, GPL, DTL
DOREAD:     LD	HL, FD_U            ; COPY THE U, C, H, R, N,EOT,GPL,DTL FROM FD
            LD	DE, RDAT_U
            LD	BC, 8
            LDIR
            LD	B, RDAT_L
            LD	DE, RDAT
            CALL	SEND_CMD
            CALL	ISDONE
            LD	B, RRDAT
            CALL	RDRSLT
            RET
            ; READ ID
CMD_READID: LD	B, RDID_L
            LD	DE, RDID
            CALL	SEND_CMD
            CALL	ISDONE
            LD	B, RRDID
            CALL	RDRSLT
            RET
DOWRITE:     ; WRITE DATA
            LD	HL, FD_N            ; COPY THE N,SC,GP FROM FD
            LD	DE, WDAT_N
            LD	BC, 3
            LDIR
            LD	A, (FD_EOT)         ; EOT is going to be FROM FD
            DEC	DE                  ; REWIND TO RDAT_EOT
            DEC	DE
            LD	(DE), A             ; Store it
            LD	B, WDAT_L
            LD	DE, WDAT
            CALL	SEND_CMD
            CALL	ISDONE
            LD	B, RWDAT
            CALL	RDRSLT
            RET
            ;
            ; FORMAT A DISK
            ; A Format Gap
            ; B Cylinder
            ; C Sectors per track
            ; D Head
            ; E N
DOFMT:      CALL	DODMA           ; SET THE DMA ADDRESS
            RST	28H
            DB	"FORMAT", CR, LF, 0
            CALL DISK_INFO
            LD	B, 0                ; CYLINDER 0
NEXTCYL:    LD	D, 0                ; HEAD 0 -> D
NEXTHEAD:   LD	A, (FD_GF)          ; Format gap -> A
FMTTRACK:
            PUSH	BC
            CALL	FMTBUF          ; FILL DMA MEMORY WITH FORMAT DATA
            POP	BC                  ; FRESHEN BC
            PUSH	BC
            LD	A, B                ; TRACK IN A
            CALL	DOSEEK          ; MOVE THE HEAD
            POP	BC
            PUSH	BC              ; REFRESH BC
            CALL	FMT1T           ; FORMAT ONE TRACK
            POP	BC
            INC	B
            LD	A, (FD_T)
            CP	B
            JR	NZ, FMTTRACK
            RET
FMTBUF:     ; Setup formatting buffer in DMA memory
            ; Each sector formatting will isue 4 DMA requests
            ;   cylinder 
            ;   head
            ;   sector
            ;   n
            ; Register parameters
            ;   B = cylinder.
            ;   D = head number.
            ;   Uses: HL,DE,BC,A
            LD	A, (FD_N)           ; Get N
            LD	E, A                ;   in E
            LD	A, (FD_SC)          ; GET SC
            LD	C, A                ;   in C
            XOR	A                   ; Clear A as sector number
            LD	HL, RAMSTA          ; HL to DMA buffer
SECTORB:    LD	(HL), B  		    ; Store cylinder
            INC	HL
            LD	(HL), D		        ; Store head
            INC	HL
            LD	(HL), A             ; Store sector
            INC	HL
            LD	(HL), E      		; Store N field
            INC	HL
            INC A                   ; Next sector
            CP	C                   ; A < Sectors per track
            JR	NZ, SECTORB    		; Loop if track buffer not complete
            RET
FMT1T:      ;
            ; FORMAT 1 TRACK
            ;   B =  cylinder
            ;   Uses: A,BC,DE,HL
            ;
            LD	A, B
            CALL	B2HEX
            LD	A, 20H
            CALL	CHOUT
            LD	HL, FD_N            ; COPY THE N,SC,GP FROM FD
            LD	DE, FMT_N           ;   to fmt_n command buffer
            LDI                     ; copy (HL) to (DE) FD_N
            LDI                     ; and the next one  FD_SC
            INC	HL                  ; skip FD_GP
            LDI                     ; and the next one  FD_GF       
            LD	B, FMT_L            ; Format command length in B
            LD	DE, FMT             ; DE has the address of the command data
            CALL	SEND_CMD
            CALL	ISDONE
            LD	B, RFMT
            CALL	RDRSLT
            RET
RDY2WR:     ;
            ; Loop till ready to write to FDCD
            ; Uses: A,F
            ;
            IN	A, (FDCS)           ; Read Status Register
IF          DEBUG
            CALL	PRINST 	        ; Print it
ENDIF
            AND	SRBits              ; Check bits 7 & 8
            CP	SRWrt               ; Ready to write?
            JR	NZ, RDY2WR
            RET

ISDONE:     IN	A, (INTS)
            CALL    PRININ
            OR	A
            JP	P, ISDONE
            RET


RDY2RD:     ;
            ; Loop till ready to read FDCD
            ; Uses: A
            ;
            IN	A, (FDCS)	        ; Read Status Register
IF          DEBUG
            CALL	PRINST		    ; Print it
ENDIF
            AND	SRBits
            CP	SRRd
            JR	NZ, RDY2RD
            RET
SEND_CMD:   ;
            ; SEND THE COMMAND AND ADD MFM BITS
		        ;   B       COMMAND BYTES LEN
		        ;   (DE)    COMMAND BUFFER
		        ; Uses: A,C,HL,F
            ;
            LD  A, (FD_U)           ; SET UNIT to FD_U
            LD  C, A                ; ALWAYS HEAD 0
            LD	A, (DE)  		        ; Load COMMAND CODE
            LD	HL, FD_DEN
            OR	A, (HL)             ; SET DENSITY BIT 6
            LD	(DE), A             ; SAVE COMMAND

            INC	DE
            LD	A, C
            LD	(DE), A
            DEC	DE
SEND_CBUF:  ;
            ; SEND THE COMMAND BUFFER TO DISK1
            ;   B       COMMAND LENGTH
            ;   (DE)    COMMAND BUFFER
            ; Uses: A,F
            ;
            CALL	RDY2WR
            LD	A, (DE)  		    ; Load COMMAND CODE
IF          DEBUG
            CALL	PRINWR
ENDIF
            OUT	(FDCD), A      		; Send it
            INC	DE		            ; args++
            DJNZ	SEND_CBUF
            RET
            ;
	        ; READ RESULTS BLOCK
		    ;   B       ARGC
            ;
RDRSLT:     CALL	RDY2RD          ; Wait till data ready
            IN	A, (FDCD)		    ; Reads the data register
IF          DEBUG
            CALL	PRINRD		    ; Display it
ENDIF
            DJNZ	RDRSLT
            RET
IF          DEBUG
PRINST:     ; Print the status in A
            ; Uses: C, F
            BIT	LOGST, (IX + 0)         ; RETURN IF DEBUG = 0
            RET	Z
            LD	C, A
            LD	A, 's'
            CALL	CHOUT
            LD	A, C
            JP	PRINTA
PRINRD:     ; Print the accumulator preceded with 'r'
            ; Uses: C,F
            BIT	LOGRD, (IX + 0)         ; RETURN IF DEBUG = 0
            RET	Z
            LD	C, A
            LD	A, 'r'
            CALL	CHOUT
            LD	A, C
            JP	PRINTA
PRINWR:     ; Print the accumulator preceded by 'w'
            ; Usea C,F
            BIT	LOGWR, (IX + 0)         ; RETURN IF DEBUG = 0
            RET	Z
            LD	C, A
            LD	A, 'W'
            CALL	CHOUT
            LD	A, C
            JP	PRINTA
PRININ:     ; Print the interrupt status in A
            ; Uses: C, F
            BIT	LOGINT, (IX + 0)         ; RETURN IF DEBUG = 0
            RET	Z
            LD	C, A
            LD	A, 'i'
            CALL	CHOUT
            LD	A, C
            JP	PRINTA
ENDIF
;
; DISK1 READ ONLY COMMANDS
;
DROM:
ONDMA:      DB	0x00                ; Extended address
            DB	RAMSTA / 256        ; DMA to 0x0800
            DB	RAMSTA & 0xff       ; Least significant byte
NDMA_L      EQU	$-ONDMA

OSPDMA:     DB	F_SPEC              ; Specify command
            DB	(SRT * 16) | HUT
            DB	(HLTT * 2) | NND    ; NOT NOT DMA = DMA
SPEC_L      EQU	$-OSPDMA
OSPINT:     DB	F_SPEC              ; Specify command
            DB	(SRT * 16) | HUT
            DB	(HLTT * 2) | ND     ; NOT DMA

            ; RECALIBRATE
ORECAL:     DB	F_RECA
            DB	0x00
RECAL_L     EQU	$-ORECAL
            ; SENSE DISK STATUS
OSDST:      DB	F_SDST
            DB	0x00                ; HEAD SELECT 0 DRIVE 0
SDST_L      EQU	$-OSDST
RSDST       EQU	1
            ; SENSE INTERRUPT STATUS
OSIST:      DB	F_SIST
SIST_L      EQU	$-OSIST
RSIST       EQU	2
            ; SEEK
OSEEK:      DB	F_SEEK
            DB	0x00                ; HEAD SELECT 0 DRIVE 0
            DB	0x20                ; CYLINDER 32
SEEK_L      EQU	$-OSEEK
            ; READ DATA
ORDAT:      DB	F_RDAT
            DB	0x00                ; HDS DS1 DS0
            DB	0x00                ; C Cylinder
            DB	0x00                ; H Head
            DB	0x01                ; R Record (1st sector)
ORDA_N:     DB	0                   ; N 128 byte sector COPIED FROM FD_N
ORDA_E:     DB	0                   ; EOT (last sector) COPIED FROM FD_EOT
            DB	0                   ; GPL (gap length) COPIED FROM FD_GP
            DB	128                 ; DTL (data length when N=0)
RDAT_L      EQU	$-ORDAT
RRDAT       EQU	7
            ; READ ID
ORDID:      DB	F_RDID
            DB	0                   ; HDS DS1 DS0
RDID_L      EQU	$-ORDID
RRDID       EQU	7
            ; WRITE DATA
OWDAT:      DB	F_WDAT
            DB	0x00                ; HDS DS1 DS0
            DB	0x00                ; C Cylinder
            DB	0x00                ; H Head
            DB	0x01                ; R Record (1st sector)
OWDA_N:     DB	0                   ; N 128 byte sector COPIED FROM FD_N
OWDA_E:     DB	0                   ; EOT (last sector) COPIED FROM FD_EOT
            DB	0                   ; GPL (gap length) COPIED FROM FD_GP
            DB	128                 ; DTL (data length when N=0)
WDAT_L      EQU	$-OWDAT
RWDAT       EQU	7
            ; FORMAT
OFMT:       DB	F_FMT               ; Command
            DB	0                   ; HDS DS1 DS0
OFMT_N:     DB	0                   ; COPIED FROM FD_N
OFMT_S:     DB	0                   ; COPIED FROM FD_SC
OFMT_G:     DB	0                   ; COPIED FROM FD_GF
            DB	0E5H                ; FILLER
FMT_L       EQU	$-OFMT
RFMT        EQU	7
            ; N = BYTES/SECTOR ID
            ; 0  128
            ; 1  256
            ; 2  512
            ; 3 1024
            ;
            ; Common Formats
            ; 8"
            ; N DEN SCSZ SC#  G3  G3F
            ; 0 FM   128  26   7   27
            ; 1 FM   256  15  14   42
            ; 2 FM   512   8  27   58
            ; 3 FM  1024   4
            ; 1 MFM  256  26  14   54
            ; 2 MFM  512  15  27   84
            ; 3 MFM 1024   8  53  116
            ;
            ;
            ; 5.25" FM:125KHz MFM:250KHz
            ; N DEN SCSZ SC# G3   G3F
            ; 0 FM   128  18   7    9
            ; 0 FM   128  16  16   25
            ; 1 FM   256   8  24   48
            ; 2 FM   512   4  70  135
            ;
            ; 1 MFM  256  18  10   12
            ; 1 MFM  256  16  32   50
            ; 2 MFM  512   8  42   80
            ; 3 MFM 1024   4 128  240
            ; PC
            ; 2 MFM  512   9  42   80 40T 2S 
            ;
            ; Atari 810
            ; 0 FM   128  18   ?   ?
            ; Atari 1050 Enhanced
            ; 0 MFM  128  26   ?   ?
            ; BBC SSSD
            ; 1 FM   256  10   ?   ?
            ; 
;             ; N TO GPL AND SECTORS LOOKUP TABLE
;             ; 5.25" SD
;             ;N   0,  1,   2,   3
; ; 5.25 SD   ;-------------------
; NSD5SC:     DB	16,  8,   4,   0
; NSD5GP:     DB	16, 24,  70,   0
; NSD5GF:     DB	25, 48, 135,   0
; ; 5.25 DD
; NDD5SC:     DB	 0, 16,   8,   4
; NDD5GP:     DB	 0, 32,  42, 128
; NDD5GF:     DB	 0, 50,  80, 240
            ; END OF ROM DATA
LOGPAD:     DS	1
DROMEND:
DROML       EQU	(DROMEND - DROM)
;
; RAM ADDRESSES
;


            ;
            ; RAM WORKSPACE DEFINITIONS
            ; THIS BIN ISN'T USED, BUT GENERATES RAM ADDRESSES FOR LINKER BUILDING ROM
            ;
; WORKING RAM
    SECTION	DATA_RW
            INCLUDE	"global.asm"
RAMWORKS:
            DB	"WORKRAM"
CMD:        DS	1, 0FFH
ARGC:       DS	1, 0FFH
ARG1:       DS	2, 0FFH
ARG2:       DS	2, 0FFH
ARG3:       DS	2, 0FFH
NUM:        DS	3, 0FFH             ; 0 - DIGITS IN A NUMBER 1,2 VALUE
RAMWIP:                             ; MEMORY IS CLEARED FROM RAMWORK UP TO HERE
STACKEND:
            DS	26H, 0FFH
STACK:      DS	2, 0FFH
ST_UNFL:
            DS	2, 0FFH
UARTSPD:    DS	1, 0FFH
BUFSIZ      EQU	32                  ; Extended for Intel Hex
BUFFER:     DS	BUFSIZ, 0FFH
BUFLEN:     DS	1, 0FFH
CHECKSUM:   DS	1, 0FFH             ; Maintain sum of characters
HEXERRS:    DS	1, 0FFH             ; Count of lines with errors
HEXEXIT:    DS	1, 0FFH             ; Stop processing when non zero
;
FD_DEN:     DS	1, 0FFH             ; Bit 6: Density 0 FM SD, 1 MFM DD
FD_U:       DS	1, 0FFH             ; Unit number in BITS 1 & 0
FD_C:       DS	1, 0FFH             ; Cylinder number
FD_H:       DS	1, 0FFH             ; Number of heads in BIT 2, 0x0=1head, 0x4=2head
FD_R:       DS  1, 0FFH             ; Sector number
FD_N:       DS	1, 0FFH             ; Holds the calculated value of N 
FD_EOT:     DS  1, 0FFH             ; EOT FOR READ DATA COMMAND
FD_GP:      DS	1, 0FFH             ; UPD_FD SETS GAP
FD_DTL:     DS	1, 0FFH             ; Data Length of data to read/write
;
FD_T:       DS	1, 0FFH             ; Number of tracks held here
FD_SC:      DS	1, 0FFH             ; UPD_FD SETS Sector Count FROM LOOKUP TABLE
FD_GF:      DS	1, 0FFH             ; UPD_FD SETS Format Gap from table
DRAM:
NDMA:       DS	NDMA_L              ; Copy size of ROM equivalent
SPDMA:      DS	SPEC_L
SPINT:      DS	SPEC_L
RECAL:      DS	RECAL_L
SDST:       DS	SDST_L
SIST:       DS	SIST_L
SEEK:       DS	SEEK_L
RDAT:       DS	RDAT_L
RDAT_U      EQU	RDAT + 1
RDID:       DS	RDID_L
WDAT:       DS	WDAT_L
WDAT_N      EQU	WDAT + 5
FMT:        DS	FMT_L
FMT_N       EQU	FMT + 2
RSD5SC:     DS	3
RSD5GP:     DS	3
RSD5GF:     DS	3
            ; 5.25" DD
RDD5SC:     DS	3
RDD5GP:     DS	3
RDD5GF:     DS	3
LOG:        DS	1
;ASSERT LOG == 0FFFH
DRAMEND:
DRAML       EQU	(DRAM - DRAMEND)
;assert  DROML = DRAML 
RAMWORKE:
RAMWORKL    EQU	RAMWORKE - RAMWORKS
