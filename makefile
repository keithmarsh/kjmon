ts = $(shell date +%Y%m%dT%H%M)
tl = $(shell date +%H%M)
FLPY_SIZE = "8"
# FLPY_SIZE = "525"
kjmon.hex: bin/memory_ROM.ihx
		cp bin/memory_ROM.ihx kjmon.hex

bin/memory_ROM.ihx: bin/memory_ROM.bin makefile
		z88dk-appmake +rom --binfile bin/memory_ROM.bin --ihex --filler 0xff --org 0x0000 --rombase 0x0000 --romsize 0x0800

bin/memory_ROM.bin: mon.o
		z88dk-z80asm -b -v -m -debug -Obin -f0FFH memory.o mon.o

mon.o: mon.asm memory.asm global.asm
ifdef CI_PIPELINE_IID
		echo "S_WELC: DB CR,LF,\"KJMon ${CI_PIPELINE_IID} ${CI_COMMIT_REF_SLUG} ${CI_JOB_STARTED_AT}\",CR,LF,0" > version.inc
else
		echo S_WELC: DB CR,LF,\"$(ts) $(FLPY_SIZE)\",CR,LF,0 > version.inc
endif
		z88dk-z80asm -l -v -DFLPY_SIZE=${FLPY_SIZE} -s memory.asm mon.asm 

dis:
		z88dk-dis -x mon.sym -mz80 -o 0 bin/memory_ROM.bin | tee mon.dis.asm

clean:
		rm -f *.o *.sym *.lis *.map bin/* kjmon.hex
